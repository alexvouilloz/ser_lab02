package controllers;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import models.*;
import sun.util.locale.provider.DateFormatProviderImpl;
import views.*;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;

public class ControleurMedia {

    private ControleurGeneral ctrGeneral;
    private static MainGUI mainGUI;
    private ORMAccess ormAccess;

    private GlobalData globalData;

    public ControleurMedia(ControleurGeneral ctrGeneral, MainGUI mainGUI, ORMAccess ormAccess) {
        this.ctrGeneral = ctrGeneral;
        ControleurMedia.mainGUI = mainGUI;
        this.ormAccess = ormAccess;
    }

    public void sendJSONToMedia() {
        new Thread() {
            public void run() {
                mainGUI.setAcknoledgeMessage("Envoi JSON ... WAIT");
                //long currentTime = System.currentTimeMillis();
                try {
                    globalData = ormAccess.GET_GLOBAL_DATA();
                    final JsonObject projectionsJson = createJsonsFromProjections(globalData.getProjections());
                    final String projectionsJsonString = new Gson().toJson(projectionsJson);

                    saveStringToFile(projectionsJsonString, "global_data.json");

                } catch (Exception e) {
                    mainGUI.setErrorMessage("Construction XML impossible", e.toString());
                }
            }
        }.start();
    }

    private JsonObject createJsonsFromProjections(List<Projection> projections) {
        JsonArray jsonProjectionsArray = new JsonArray();
        for (Projection proj : projections) {

            JsonObject jsonProj = new JsonObject();

            jsonProj.addProperty("nomFilm", proj.getFilm().getTitre());

            boolean premierRoleFound = false;
            boolean secondRoleFound = false;
            for (RoleActeur role : proj.getFilm().getRoles()) {
                if (role.getPlace() == 1) {
                    jsonProj.addProperty("premierRole", role.getActeur().getNom());
                    premierRoleFound = true;
                } else if (role.getPlace() == 2) {
                    jsonProj.addProperty("secondRole", role.getActeur().getNom());
                    secondRoleFound = true;
                }
                if (premierRoleFound && secondRoleFound) {
                    break;
                }
            }
            jsonProj.addProperty("dateProjection", proj.getDateHeure().getTime().toLocaleString());
            jsonProjectionsArray.add(jsonProj);
        }

        JsonObject jsonProjections = new JsonObject();
        jsonProjections.add("projections", jsonProjectionsArray);
        return jsonProjections;
    }

    public static void saveStringToFile(String string, String fileName) throws Exception {
        BufferedWriter bw = new BufferedWriter(
                new OutputStreamWriter(new FileOutputStream(fileName), "UTF-8"));
        bw.write(string);
        bw.flush();
        bw.close();
    }
}