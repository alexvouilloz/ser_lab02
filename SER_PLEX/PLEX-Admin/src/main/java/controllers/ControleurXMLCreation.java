package controllers;

import models.*;
import org.jdom2.DocType;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.XMLOutputter;
import views.*;

import java.io.*;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import com.thoughtworks.xstream.XStream;

public class ControleurXMLCreation {

	//private ControleurGeneral ctrGeneral;
	private static MainGUI mainGUI;
	private ORMAccess ormAccess;

	private GlobalData globalData;

	public ControleurXMLCreation(ControleurGeneral ctrGeneral, MainGUI mainGUI, ORMAccess ormAccess){
		//this.ctrGeneral=ctrGeneral;
		ControleurXMLCreation.mainGUI=mainGUI;
		this.ormAccess=ormAccess;
	}

	public void createXML(){
		new Thread(){
				public void run(){
					mainGUI.setAcknoledgeMessage("Creation XML... WAIT");
					long currentTime = System.currentTimeMillis();
					try {
						globalData = ormAccess.GET_GLOBAL_DATA();
					}
					catch (Exception e){
						mainGUI.setErrorMessage("Acquisition des données impossible", e.toString());
						return;
					}

					Document gdXML = new Document(creerArboresence(globalData.getProjections()));
					gdXML.setDocType(new DocType("bdd", "projections.dtd"));

					XMLOutputter xmlOutputter = new XMLOutputter();
					try {
						xmlOutputter.output(gdXML, new FileOutputStream("global_data.xml"));
					} catch(Exception exception) {
						mainGUI.setErrorMessage("Construction XML impossible", exception.toString());
					}
				}
		}.start();
	}

	public void createXStreamXML(){
		new Thread(){
				public void run(){
					mainGUI.setAcknoledgeMessage("Creation XML... WAIT");
					long currentTime = System.currentTimeMillis();
					try {
						globalData = ormAccess.GET_GLOBAL_DATA();
						globalDataControle();
					}
					catch (Exception e){
						mainGUI.setErrorMessage("Construction XML impossible", e.toString());
					}

					XStream xstream = new XStream();
					writeToFile("global_data.xml", xstream, globalData);
					System.out.println("Done [" + displaySeconds(currentTime, System.currentTimeMillis()) + "]");
					mainGUI.setAcknoledgeMessage("XML cree en "+ displaySeconds(currentTime, System.currentTimeMillis()) );
				}
		}.start();
	}

	private static void writeToFile(String filename, XStream serializer, Object data) {
		try {
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filename), "UTF-8"));
			serializer.toXML(data, out);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static final DecimalFormat doubleFormat = new DecimalFormat("#.#");
	private static final String displaySeconds(long start, long end) {
		long diff = Math.abs(end - start);
		double seconds = ((double) diff) / 1000.0;
		return doubleFormat.format(seconds) + " s";
	}

	private void globalDataControle(){
		for (Projection p:globalData.getProjections()){
			System.out.println("******************************************");
			System.out.println(p.getFilm().getTitre());
			System.out.println(p.getSalle().getNo());
			System.out.println("Acteurs *********");
			for(RoleActeur role : p.getFilm().getRoles()) {
				System.out.println(role.getActeur().getNom());
			}
			System.out.println("Genres *********");
			for(Genre genre : p.getFilm().getGenres()) {
				System.out.println(genre.getLabel());
			}
			System.out.println("Mot-cles *********");
			for(Motcle motcle : p.getFilm().getMotcles()) {
				System.out.println(motcle.getLabel());
			}
			System.out.println("Langages *********");
			for(Langage langage : p.getFilm().getLangages()) {
				System.out.println(langage.getLabel());
			}
			System.out.println("Critiques *********");
			for(Critique critique : p.getFilm().getCritiques()) {
				System.out.println(critique.getNote());
				System.out.println(critique.getTexte());
			}
		}
	}

	/**
	 * Méthode construisant toute l'arboresence du document XML.
	 *
	 * @param projections Liste de toutes les projections
	 * @return Element racine contenant toute l'arborescence
	 */
	private Element creerArboresence(List<Projection> projections) {
		Element racine = new Element("bdd");

		racine.addContent(creerProjections(projections));
		racine.addContent(creerSalles(projections));
		racine.addContent(creerFilms(projections));
		racine.addContent(creerActeurs(projections));

		return racine;
	}

	private Element creerProjections(List<Projection> projections) {
		/* Normalement, chaque projection est unique */

		Element projectionsElement = new Element("projections");

		for(Projection p: projections) {
			Element projectionElement = new Element("projection");

			projectionElement.setAttribute("id", "projection_" + Long.toString(p.getId()));
			projectionElement.setAttribute("idSalle", "salle_" + Long.toString(p.getSalle().getId()));
			projectionElement.setAttribute("idFilm", "film_" + Long.toString(p.getFilm().getId()));
			projectionElement.addContent(new Element("dateHeure").setText(p.getDateHeure().getTime().toString()));

			projectionsElement.addContent(projectionElement);
		}

		return projectionsElement;
	}

	private Element creerFilms(List<Projection> projections) {
		Map<Long, Object> filmsPresents = new HashMap<>();

		Element filmsElement = new Element("films");
		for(Projection p: projections) {
			Film film = p.getFilm();
			if(filmsPresents.containsKey(film.getId())) {
				continue;
			}

			filmsPresents.put(film.getId(), null);

			Element filmElement = new Element("film");

			filmElement.setAttribute("id", "film_" + Long.toString(film.getId()));
			filmElement.addContent(new Element("titre").setText(film.getTitre()));
			filmElement.addContent(new Element("synopsis").setText(film.getSynopsis()));
			filmElement.addContent(new Element("duree").setText(Integer.toString(film.getDuree())));
			filmElement.addContent(new Element("photo").setText(film.getPhoto()));
			filmElement.addContent(new Element("roles").addContent(creerRoles(film)));
			filmElement.addContent(new Element("critiques").addContent(creerCritiques(film)));
			filmElement.addContent(new Element("motCles").addContent(creerMotsCles(film)));
			filmElement.addContent(new Element("genres").addContent(creerGenres(film)));
			filmElement.addContent(new Element("langages").addContent(creerLangages(film)));

			filmsElement.addContent(filmElement);
		}

		return filmsElement;
	}

	private Element creerSalles(List<Projection> projections) {
		Map<Long, Object> sallesPresentes = new HashMap<>();

		Element sallesElement = new Element("salles");
		for(Projection p: projections) {
			Salle salle = p.getSalle();
			if(sallesPresentes.containsKey(salle.getId())) {
				continue;
			}

			sallesPresentes.put(salle.getId(), null);

			Element salleElement = new Element("salle");

			salleElement.setAttribute("id", "salle_" + Long.toString(salle.getId()));
			salleElement.addContent(new Element("no").setText(salle.getNo()));
			salleElement.addContent(new Element("taille").setText(Integer.toString(salle.getTaille())));

			sallesElement.addContent(salleElement);
		}

		return sallesElement;
	}

	private Element creerActeurs(List<Projection> projections) {
		Map<Long, Object> acteursPresents = new HashMap<>();

		Element acteursElement = new Element("acteurs");

		for(Projection p : projections) {
			Set<RoleActeur> rolesFilm = p.getFilm().getRoles();

			for(RoleActeur roleActeur : rolesFilm) {
				Acteur acteur = roleActeur.getActeur();

				if(acteursPresents.containsKey(acteur.getId())) {
					continue;
				}

				acteursPresents.put(acteur.getId(), null);

				Element acteurElement = new Element("acteur");

				acteurElement.setAttribute("id", "acteur_" + Long.toString(acteur.getId()));
				if(acteur.getNom() != null) {
					acteurElement.addContent(new Element("nom").setText(acteur.getNom()));
				}
				if(acteur.getNomNaissance() != null) {
					acteurElement.addContent(new Element("nomDeNaissance").setText(acteur.getNomNaissance()));
				}
				if(acteur.getBiographie() != null) {
					acteurElement.addContent(new Element("biographie").setText(acteur.getBiographie()));
				}
				if(acteur.getDateNaissance() != null) {
					String date = new SimpleDateFormat("yyyy-mm-dd").format(acteur.getDateNaissance().getTime());
					acteurElement.addContent(new Element("dateDeNaissance").setText(date));
				}
				if(acteur.getDateDeces() != null) {
					String date = new SimpleDateFormat("yyyy-mm-dd").format(acteur.getDateDeces().getTime());
					acteurElement.addContent(new Element("dateDeDeces").setText(date));
				}

				acteursElement.addContent(acteurElement);
			}
		}

		return acteursElement;
	}

	private List<Element> creerRoles(Film film) {
		List<Element> roles = new LinkedList<>();

		for(RoleActeur role : film.getRoles()) {
			Element roleElement = new Element("role");

			roleElement.setAttribute("id", "role_" + Long.toString(role.getId()));
			if(role.getActeur() != null) {
				roleElement.setAttribute("idActeur", "acteur_" + Long.toString(role.getActeur().getId()));
			}
			if(role.getPersonnage() != null) {
				roleElement.addContent(new Element("personnage").setText(role.getPersonnage()));
			}
			if(role.getPlace() != 0) {
				roleElement.addContent(new Element("place").setText(Long.toString(role.getPlace())));
			}

			roles.add(roleElement);
		}

		return roles;
	}

	private List<Element> creerMotsCles(Film film) {
		List<Element> motsCles = new LinkedList<>();

		for(Motcle motcle : film.getMotcles()) {
			Element motcleElement = new Element("motCle").setText(motcle.getLabel());
			motsCles.add(motcleElement);
		}

		return motsCles;
	}

	private List<Element> creerGenres(Film film) {
		List<Element> genres = new LinkedList<>();

		for(Genre genre : film.getGenres()) {
			Element genreElement = new Element("genre").setText(genre.getLabel());
			genres.add(genreElement);
		}

		return genres;
	}

	private List<Element> creerLangages(Film film) {
		List<Element> langages = new LinkedList<>();

		for(Langage langage : film.getLangages()) {
			Element langageElement = new Element("langage").setText(langage.getLabel());
			langages.add(langageElement);
		}

		return langages;
	}

	private List<Element> creerCritiques(Film film) {
		List<Element> critiques = new LinkedList<>();

		for(Critique critique : film.getCritiques()) {
			Element critiqueElement = new Element("critique").setText(critique.getTexte());
			critiques.add(critiqueElement);
		}

		return critiques;
	}
}